package diegoluisr;

import com.sun.javafx.webkit.WebConsoleListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML private WebView webView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        WebConsoleListener.setDefaultListener(new WebConsoleListener(){
            @Override
            public void messageAdded(WebView webView, String message, int lineNumber, String sourceId) {
                System.out.println("Console: [" + sourceId + ":" + lineNumber + "] " + message);
            }
        });

        WebEngine engine = webView.getEngine();
        engine.load("https://www.youtube.com/watch?v=7YDPNl7PeUU");
    }
}
